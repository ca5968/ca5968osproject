#include <pthread.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex>
#include <vector>
#include <sstream>


void *PinnacleFunction(void *pMessage) {
    std::string *msg2 = reinterpret_cast<std::string*>(pMessage);
    std::string part3 = *msg2;

    //check if part3 contains negative numbers
    if (part3.find("-") != std::string::npos) {
        std::cout << "Pinnacle Output: Messages cannot contain negative numbers." << std::endl;
        pthread_exit(NULL);
        return (void *)1;
    }

    //check if part3 starts with alphabetical characters
    std::match_results<const char*> s1Match;
    const char *tempPart3 = part3.c_str();
    if (std::regex_search(tempPart3, s1Match, std::regex("(^[a-zA-Z]+\\s)")) &&
        s1Match.size() > 1) {
        std::string s1 = s1Match.str(1);  //Section 1

        part3.erase(0, s1.size());
        s1.pop_back();

        if (s1.size() % 3 != 0) {
            std::cout << "Pinnacle Output: Section 1 length is not divisible by 3." << std::endl;
            pthread_exit(NULL);
            return (void *)1;
        }
        //save Section 2 as vector of ints
        std::vector<int> s2;
        std::stringstream stream(part3);
        int t;
        while (stream >> t) {
            s2.push_back(t);
        }

        if (s2.size() < 9) {
            std::cout << "Pinnacle Output: Section 2 does not contain at least 9 digits." << std::endl;
            pthread_exit(NULL);
            return (void *)1;
        }
        //ignore any extra digits
        while (s2.size() > 9) {
            s2.pop_back();
        }
        //produce deciphered Part 2
        std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        std::string pinnacleResult;
        int i;
        for (i = 0; i < s1.size(); i += 3) {
            int c[3] = { static_cast<int>(alphabet.find(toupper(s1[i]))),
                         static_cast<int>(alphabet.find(toupper(s1[i+1]))),
                         static_cast<int>(alphabet.find(toupper(s1[i+2])))};
            int nC[3] = {(s2[0]*c[0] + s2[1]*c[1] + s2[2]*c[2]),
                         (s2[3]*c[0] + s2[4]*c[1] + s2[5]*c[2]),
                         (s2[6]*c[0] * s2[7]*c[1] + s2[8]*c[2])};
            pinnacleResult += alphabet[nC[0] % 26];
            pinnacleResult += alphabet[nC[1] % 26];
            pinnacleResult += alphabet[nC[2] % 26];
        }

        std::cout << "Pinnacle Output: " << pinnacleResult << std::endl;
    }
    else {
        std::cout << "Pinnacle Output: Section 1 contains non-alphabetical characters." << std::endl;
        pthread_exit(NULL);
        return (void *)1;
    }

    pthread_exit(NULL);
    return (void *)1;
}

void *HillFunction(void *hMessage) {
    std::string *msg2 = reinterpret_cast<std::string*>(hMessage);
    std::string part2 = *msg2;

    //check if part2 contains negative numbers
    if (part2.find("-") != std::string::npos) {
        std::cout << "Hill Output: Messages cannot contain negative numbers." << std::endl;
        pthread_exit(NULL);
        return (void *)1;
    }
    //check if part2 starts with alphabetical characters
    std::match_results<const char*> s1Match;
    const char *tempPart2 = part2.c_str();
    if (std::regex_search(tempPart2, s1Match, std::regex("(^[a-zA-Z]+\\s)")) &&
        s1Match.size() > 1) {
        std::string s1 = s1Match.str(1);  //Section 1

        part2.erase(0, s1.size());
        s1.pop_back();

        if (s1.size() % 2 != 0) {
            std::cout << "Hill Output: Section 1 does not contain an even number of characters." << std::endl;
            pthread_exit(NULL);
            return (void *)1;
        }
        //save Section 2 as vector of ints
        std::vector<int> s2;
        std::stringstream stream(part2);
        int t;
        while (stream >> t) {
            s2.push_back(t);
        }

        if (s2.size() < 4) {
            std::cout << "Hill Output: Section 2 does not contain at least 4 digits." << std::endl;
            pthread_exit(NULL);
            return (void *)1;
        }
        //ignore any extra digits
        while (s2.size() > 4) {
            s2.pop_back();
        }
        //produce deciphered Part 2
        std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        std::string hillResult;
        int i;
        for (i = 0; i < s1.size(); i+=2) {
            int c[2] = {static_cast<int>(alphabet.find(toupper(s1[i]))),
                        static_cast<int>(alphabet.find(toupper(s1[i+1])))};
            int nC[2] = {(s2[0]*c[0] + s2[1]*c[1]), (s2[2]*c[0] + s2[3]*c[1])};
            hillResult += alphabet[nC[0] % 26];
            hillResult += alphabet[nC[1] % 26];
        }

        std::cout << "Hill Output: " << hillResult << std::endl;
    }
    else {
        std::cout << "Hill Output: Section 1 contains non-alphabetical characters." << std::endl;
        pthread_exit(NULL);
        return (void *)1;
    }

    pthread_exit(NULL);
    return (void *)1;
}

void *FenceFunction(void *fMessage) {
    std::string *msg2 = reinterpret_cast<std::string*>(fMessage);
    std::string part1 = *msg2;

    //check if part1 contains negative numbers
    if (part1.find("-") != std::string::npos) {
        std::cout << "Fence Output: Messages cannot contain negative numbers." << std::endl;
        pthread_exit(NULL);
        return (void *)1;
    }

    //check if part1 begins with a digit
    std::match_results<const char*> digitMatch;
    const char *tempPart1 = part1.c_str();
    if (std::regex_search(tempPart1, digitMatch, std::regex("(^[1-9]+)")) &&
        digitMatch.size() > 1) {

        std::string digStr = digitMatch.str(1);
        size_t found;
        int i;
        for (i = 0; i < digStr.size(); i++) {
            found = digStr.find(std::to_string(i+1));
            if (found != std::string::npos) {         //if digStr contains i
                found = digStr.find(std::to_string(i+1), found+1);
                if (found != std::string::npos) {         //if digStr contains more than 1 i
                    digStr.erase(found, digStr.size()-1);       //delete digits after second i
                }
            }
            else {
                std::cout << "Fence Output: All required digits for Part 1 are not present, or it contained a zero." << std::endl;
                pthread_exit(NULL);
                return (void *)1;
            }
        }

        found = part1.find(digStr);
        if (found != std::string::npos) {
            part1.erase(found, digStr.size()); //delete digStr from beginning of part1
        }
        else {
            std::cout << "Fence Output: Somehow digStr is not in part 1." << std::endl;
            pthread_exit(NULL);
            return (void *)1;
        }

        //make sure part1.size() is divisible by digStr.size()
        while (part1.size() % digStr.size() != 0) {
            part1 += " ";
        }

        int k = part1.size() / digStr.size();

        //create vector of strings to act as matrix
        std::vector<std::string> vectrix(digStr.size());
        int j = 0;
        for (i = 0; i < digStr.size(); i++) {
            vectrix[i] = part1.substr(j, k);
            j += k;
        }

        //produce deciphered Part 1
        std::string fenceResult;
        for (i = 0; i < k; i++) {
            for (j = 0; j < digStr.size(); j++) {
                fenceResult += vectrix[(digStr[j] - '0') - 1][i];
            }
        }

        std::cout << "Fence Output: " << fenceResult << std::endl;

    }
    else {
        std::cout << "Fence Output: Part 1 did not begin with digits." << std::endl;
    }

    pthread_exit(NULL);
    return (void *)1; //could maybe just do return;
}


void *DecoderFunction(void *message) {
    std::string *msg2 = reinterpret_cast<std::string*>(message);
    std::string msg = *msg2;

    std::string part3;
    std::match_results<const char*> match3;
    const char *tempMsg = msg.c_str();
    if (std::regex_search(tempMsg, match3, std::regex("(\\*\\*\\*[\\w\\s\\d]+)")) &&
        match3.size() > 1) {
        part3 = match3.str(1);

        size_t found = msg.find(part3);
        msg.erase(found, part3.size());

        part3.erase(0, 3); //gets rid of asterisks

        std::string part2;
        std::match_results<const char*> match2;
        if (std::regex_search(tempMsg, match2, std::regex("(\\*\\*[\\w\\s\\d]+)")) &&
            match2.size() > 1) {
            part2 = match2.str(1);

            found = msg.find(part2);
            msg.erase(found, part2.size());

            part2.erase(0, 2);
            msg.erase(0, 1);

            pthread_t fenceID;
            pthread_t hillID;
            pthread_t pinnacleID;
            void *fencePointer;
            void *hillPointer;
            void *pinnaclePointer;
            std::string *prt1str = new std::string(msg);
            std::string *prt2str = new std::string(part2);
            std::string *prt3str = new std::string(part3);
            int fenceResult = pthread_create(&fenceID, NULL, FenceFunction, (void *)prt1str);
            int hillResult = pthread_create(&hillID, NULL, HillFunction, (void *)prt2str);
            int pinnResult = pthread_create(&pinnacleID, NULL, PinnacleFunction, (void *)prt3str);
            if (fenceResult == 0) {
                fenceResult = pthread_join(fenceID, &fencePointer);
                if (fenceResult != 0) {
                    std::cout << "Something went wrong joining Fence" << std::endl;
                }
            }
            else {
                std::cout << "Something went wrong creating Fence" << std::endl;
            }
            if (hillResult == 0) {
                hillResult = pthread_join(hillID, &hillPointer);
                if (hillResult != 0) {
                    std::cout << "Something went wrong joining Hill" << std::endl;
                }
            }
            else {
                std::cout << "Something went wrong creating Hill" << std::endl;
            }
            if (pinnResult == 0) {
                pinnResult = pthread_join(pinnacleID, &pinnaclePointer);
                if (pinnResult != 0) {
                    std::cout << "Something went wrong joining Pinnacle" << std::endl;
                }
            }
            else {
                std::cout << "Something went wrong creating Pinnacle" << std::endl;
            }
        }
        else {
            std::cout << "Decoder could not find part 2" << std::endl;
        }

    }
    else {
        std::cout << "Decoder could not find part 3" << std::endl;
    }

    pthread_exit(NULL);
    return (void *)1;
}

void *SifterFunction(void *s1){
    pthread_t decoderID;

    void *decoderPointer;

    int numTries = 0;
    std::string msg;
    while (numTries < 3){
        std::cout << "Enter message to be decrypted, or enter Quit to exit: ";
        std::getline(std::cin, msg);
        if (msg == "Quit" || msg == "quit") {
            break;
        }
        size_t astNum = count(msg.begin(), msg.end(), '*');

        if (std::regex_search(msg, std::regex("([^\\*]+\\*[\\w\\s\\d]+)|(^\\*[\\w\\s\\d]+)")) &&
            std::regex_search(msg, std::regex("([^\\*]+\\*\\*[\\w\\s\\d]+)|(^\\*\\*[\\w\\s\\d]+)")) &&
            std::regex_search(msg, std::regex("(\\*\\*\\*[\\w\\s\\d]+)")) &&
            (astNum == 6)) {

            numTries = 0;
            std::cout << "Original message: " << msg << std::endl;

            std::string *message = new std::string(msg);

            int result;

            if (pthread_create(&decoderID, NULL, DecoderFunction, (void *)message) == 0) {
                result = pthread_join(decoderID, &decoderPointer);
                if (result != 0) {
                    std::cout << "Something went wrong joining Decoder" << std::endl;
                }
            }
            else {
                std::cout << "Something went wrong creating Decoder" << std::endl;
            }
        }
        else {
            numTries++;
            std::cout << "Invalid message. You have " << 3 - numTries << " tries left." << std::endl;
        }
    }
    if (numTries > 2) {
        std::cout << "No more tries left. Program closing.";
    }

    pthread_exit(NULL);
    return (void *)1;
}



int main() {
    pthread_t sifterID;
    void *sifterPointer;

    int result;

    if (pthread_create(&sifterID, NULL, SifterFunction, NULL) == 0) {
        result = pthread_join(sifterID, &sifterPointer);
        if (result != 0) {
            std::cout << "Something went wrong joining Sifter" << std::endl;
        }
    }
    else {
        std::cout << "Something went wrong creating Sifter" << std::endl;
    }

    pthread_exit(NULL);
}